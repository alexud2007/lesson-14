<?php


	
	class FileStorage extends Storage {

    public function create($object) {
        $slug = $object->slug;
        $date = date('Y-m-d');

        if (file_exists($slug . '_' . $date)) {
            $i = 1;
            while (file_exists($slug . '_' . $date . '_' . $i)) {
                $i++;
            }

            $slug = $slug . '_' . $date . '_' . $i;
            $object->slug = $slug;
        } else {
            $slug = $slug . '_' . $date;
            $object->slug = $slug;
        }

        file_put_contents($slug, serialize($object));

        return $slug;
    }

    public function read($id) {
        return unserialize(file_get_contents($id));
    }

    public function update($id, $object) {
        file_put_contents($id, serialize($object));

        return true;
    }

    public function delete($id) {
        unlink($id);

        return true;
    }

    public function list() {
        $files = scandir('.');

        $texts = [];

        foreach ($files as $file) {
            if (strpos($file, '.txt') !== false) {
                array_push($texts, unserialize(file_get_contents($file)));
            }
        }

        return $texts;
    }
}