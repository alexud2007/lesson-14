<?php

    function autoloader($className)
    {
        $path = __DIR__ . '/entities/' . $className . '.php';
        if (file_exists($path)) {
            require_once $path;
        }
    }

    spl_autoload_register('autoloader');